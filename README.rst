practice project


future dev:

general:
1) adding unit tests
2) collect and analyze without pandas, with numpy only
3) move data to binary version control (artifactory?)

prediction module:
1) splitting module to classes
2) adding serialization functionality in order to save and load trained models
3) switch to model the works :)
4) create factory for Predictor class with metaclass (instead of classmethod, for practice)

