import matplotlib.pyplot as plt
import functools


def plot_timer(timeout_sec):
    """decorator function for closing plot after a time given by user"""
    def timer_decorator(func):
        # use functools.wraps to preserve the attributes of original function
        @functools.wraps(func)
        def plot_wrapper(*args, **kwargs):
            # creating a timer object and setting an interval of timeout_sec*1000 milliseconds
            fig = plt.figure()
            timer = fig.canvas.new_timer(interval=timeout_sec*1000)
            timer.add_callback(__close_event)

            func(*args, **kwargs)

            timer.start()
            plt.show()
        return plot_wrapper
    return timer_decorator


def __close_event():
    # timer calls this function after x seconds and closes the window
    plt.close()

