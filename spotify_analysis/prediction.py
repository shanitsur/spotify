from abc import ABC, abstractmethod

from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score


def train_and_run_prediction(spotify_data, column_to_predict):
    (spotify_input_train,
     spotify_input_test,
     streams_output_train,
     streams_output_test) = prepare_data(spotify_data, column_to_predict)

    model = DecisionTreeClassifier()
    model.fit(spotify_input_train, streams_output_train)
    predictions = model.predict(spotify_input_test)
    print(f"accuracy_score: {accuracy_score(streams_output_test,predictions)}")


def prepare_data(data, col_to_predict):
    """ prepare data input and output to the machine learning model"""
    spotify_input = remove_unfitting_columns(data, col_to_predict)
    streams_output = data[col_to_predict]

    return train_test_split(spotify_input, streams_output, test_size=0.02)


def remove_unfitting_columns(data, col_to_predict) -> []:
    """ removes non-numeric columns from data, and output """
    columns_to_drop = [name for name, column_type in data.dtypes.to_dict().items() if column_type in (str, object)]
    columns_to_drop.append(col_to_predict)
    return data.drop(columns=columns_to_drop)


# def predict_whether_song_will_be_streamed( train_data, test_data):
#     pass
#
#
# class Predictor(ABC):
#     @abstractmethod
#     def train(self, train_input, train_result):
#         pass
#
#     @abstractmethod
#     def predict(self, data):
#         pass
#
#
# class DecisionTreePredictor("Predictor"):
#
#     def __init__(self, model_instance):
#         self.model: type(DecisionTreeClassifier()) = model_instance
#
#     def train(self, train_input, train_result):
#         self.model.fit(train_input, train_result)
#
#     def predict(self, data):
#         return self.model.predict(data)
