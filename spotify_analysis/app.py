"""1) load tables from db
   2) query data in 2 ways - a. from db
                          b. by dataframe\
   3) use machine learning to predict data """

import functools  # reduce
from importlib import resources

import decorators

from sqlalchemy.orm import Session
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine

engine = create_engine("postgresql+psycopg2://postgres:Til2013tan?@localhost/training")
Session = sessionmaker()
Session.configure(bind=engine)
session = Session()

Base = declarative_base()




import prediction
import analysis.sql_queries as sql

from collections import Counter


def main():
    pass
    # sql.analyze(db)
    # analyze(spotify_data)
    # prediction.train_and_run_prediction(spotify_data, "streams")


def analyze(data):
    """answers 1-3 analysis questions"""
    good_correlation_pct = 75

    show_most_streamed_artist_and_his_hits(data)
    check_correlation_by_columns(data, ['in_apple_playlists', 'in_spotify_charts'],
                                 20,
                                 good_correlation_pct)


def show_most_streamed_artist_and_his_hits(data):
    """"logs and plots the answers to 1-2 analysis questions"""
    artists_by_streamed_songs = data['lead_artist'].value_counts()
    most_streamed_artist, num_songs_by_artist = artists_by_streamed_songs.idxmax(), artists_by_streamed_songs.max()

    app_logger.info(f"'{most_streamed_artist}' is most streamed artist, with {num_songs_by_artist} songs in data")

    most_streamed_artist_songs = data[data['lead_artist'] == most_streamed_artist]["track_name"]
    app_logger.info(f"The artist's songs are :{most_streamed_artist_songs.to_string()}")
    plot_most_streamed_artists(artists_by_streamed_songs)


@decorators.plot_timer(10)
def plot_most_streamed_artists(streamed_artists):
    streamed_artists[:11].plot(kind='bar',
                                        title="10th Most Streamed Artists",
                                        xlabel="artists name",
                                        ylabel="number of streamed songs")




def check_correlation_by_columns(data, columns, num_items, min_correlation_pct):
    """Checks and logs the correlation % for num_items of received columns"""
    correlation_pct = calc_correlation_by_columns(data, columns, num_items) * 100
    message = "good. Higher" if correlation_pct > min_correlation_pct else "bad. Lower"
    app_logger.info(f" The correlation % between columns is {correlation_pct}%, "
                    f"which is {message} then {min_correlation_pct}")


def calc_correlation_by_columns(data, columns, num_items):
    """Returns the correlation of x most common items in received columns.
    inputs:
    1) Data - the input dataframe
    2) list of column names to work on
    3) columns - num_items - num of the common items to include in correlation

    The function create sets of most common items for each column and calculate their intersection"""
    # can't use map due to changing length of args
    largest_items = [set(data[col].nlargest(num_items).index) for col in columns]

    common_items = functools.reduce(lambda x, y: x & y, largest_items)
    correlation = len(common_items) / num_items

    app_logger.info(f"The common items for the {num_items}"
                    f" largest items in the following columns : {columns}, are {common_items}"
                    f". the correlation between columns is {correlation}")
    return correlation




# currently not used in code flow
def find_most_common_item(my_list):
    """returns the most common item in a list and num appearances
     using count from collection

    output - tuple (item(str), num appearances (int))"""
    data = Counter(my_list)
    return data.most_common(1)[0]