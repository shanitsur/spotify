from sqlalchemy import Column, Integer, ForeignKey, Table
from sqlalchemy.orm import relationship
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column

from typing import Optional, List

# declarative base class

Base = DeclarativeBase()

tracks_artists = Table("tracks_artists",
                       Base.metadata,
                       Column("track_id", Integer, ForeignKey("Track.id")),
                       Column("artist_id", Integer, ForeignKey("Artist.id")),)


class Artist(Base):
    __tablename__ = "artists"
    id: Mapped[int] = mapped_column(primary_key=True)
    first_name: Mapped[str]
    last_name: Mapped[Optional[str]]
    tracks: Mapped[List["Track"]] = relationship(secondary=tracks_artists, back_populates="artists")


class Track(Base):
    __tablename__ = "tracks"
    id: Mapped[int] = mapped_column(primary_key=True)
    released_year: Mapped[int]
    released_month: Mapped[int]
    released_day: Mapped[int]
    in_spotify_playlists: Mapped[int]
    in_spotify_charts: Mapped[int]
    streams: Mapped[int]
    in_apple_playlists: Mapped[int]
    in_apple_charts: Mapped[int]
    in_deezer_playlists: Mapped[int]
    in_deezer_charts: Mapped[int]
    in_shazam_charts: Mapped[int]
    bpm: Mapped[int]
    key: Mapped[str]
    mode: Mapped[str]
    danceability_pct: Mapped[int]
    valence_pct: Mapped[int]
    energy_pct: Mapped[int]
    acousticness_pct: Mapped[int]
    instrumentalness_pct: Mapped[int]
    liveness_pct: Mapped[int]
    speechiness_pct: Mapped[int]
    artists: Mapped[List["Artist"]] = relationship(secondary=tracks_artists, back_populates="tracks")
