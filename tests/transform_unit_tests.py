import unittest
from context import transform

import pandas as pd


class TestTransform(unittest.TestCase):
    def setUp(self):
        raw_data = {
            "track_name": ["Seven(feat.Latto)(Explicit Ver.)",
                           "LALA",
                           "vampire",
                           "Cruel",
                           "Summer",
                           "WHERE",
                           "SHE"],
            "artist(s)_name": ["Latto",
                               "Jung Kook",
                               "Myke Towers",
                               "Eslabon Armado, Peso Pluma",
                               "Gabito Ballesteros, Junior H, Peso Pluma",
                               "Taylor Swift",
                               "Taylor Swift, bla, Eslabon Armado"],
            "artist_count": [1, 1, 1, 2, 3, 1, 3],
            "released_year": [2023, 2023, 2023, 2023, 2023, 2023, 2054]}
        self.data = pd.DataFrame(data=raw_data)

    def tearDown(self):
        pass

    def test_transform(self):
        a, b, c = transform.transform(self.data)
        print(a)
        print(b)
        print(c)


@unittest.skip("class TestSplitDataToTables not fully implemented yet")
class TestSplitDataToTables(unittest.TestCase):
    def setUp(self):
        raw_data = {
            "track_name": ["Seven(feat.Latto)(Explicit Ver.)",
                           "LALA",
                           "vampire",
                           "Cruel",
                           "Summer",
                           "WHERE",
                           "SHE"],
            "artist(s)_name": ["Latto",
                               "Jung Kook",
                               "Myke Towers",
                               "Eslabon Armado, Peso Pluma",
                               "Gabito Ballesteros, Junior H, Peso Pluma",
                               "Taylor Swift",
                               "Taylor Swift, bla, Eslabon Armado"],
            "artist_count": [1, 1, 1, 2, 3, 1, 3],
            "released_year": [2023, 2023, 2023, 2023, 2023, 2023, 2054]}

        self.data = pd.DataFrame(data=raw_data)
        self.expected_artists = [["latto", None],
                            ["jung", "kook"],
                            ["myke", "towers"],
                            ["eslabon", "armado"],
                            ["peso", "pluma"],
                            ["gabito", "ballesteros"],
                            ["junior", "h"],
                            ["taylor", "swift"],
                            ["bla", None]]
        self.expected_tracks_artists = [[0, 0],
                                        [1, 1],
                                        [2, 2],
                                        [3, 3],
                                        [3, 4],
                                        [4, 5],
                                        [4, 6],
                                        [4, 4],
                                        [5, 7],
                                        [6, 7],
                                        [6, 8],
                                        [6, 3]]

    def tearDown(self):
        pass

    def test_valid_input(self):
        artists, tracks_artists = transform.split_data_to_tables(self.data)
        print(artists)
        print(tracks_artists)


class TestCreateArtistsAndTracksArtists(unittest.TestCase):
    def setUp(self):
        raw_data = {"artist(s)_name": ["Latto",
                                       "Jung Kook",
                                       "Myke Towers",
                                       "Eslabon Armado, Peso Pluma",
                                       "Gabito Ballesteros, Junior H, Peso Pluma",
                                       "Taylor Swift",
                                       "Taylor Swift, bla, Eslabon Armado"],
                    "artist_count": [1, 1, 1, 2, 3, 1, 3],
                    "released_year": [2023, 2023, 2023, 2023, 2023, 2023, 2054]}

        self.data = pd.DataFrame(data=raw_data)
        self.expected_artists = [["latto", None],
                            ["jung", "kook"],
                            ["myke", "towers"],
                            ["eslabon", "armado"],
                            ["peso", "pluma"],
                            ["gabito", "ballesteros"],
                            ["junior", "h"],
                            ["taylor", "swift"],
                            ["bla", None]]
        self.expected_tracks_artists = [[0, 0],
                                        [1, 1],
                                        [2, 2],
                                        [3, 3],
                                        [3, 4],
                                        [4, 5],
                                        [4, 6],
                                        [4, 4],
                                        [5, 7],
                                        [6, 7],
                                        [6, 8],
                                        [6, 3]]

    def tearDown(self):
        pass

    def test_valid_input(self):
        artists, tracks_artists = transform.create_artists_and_tracks_artists(self.data)
        self.assertEqual(9, len(artists))
        self.assertEqual(self.expected_artists, artists)
        self.assertEqual(self.expected_tracks_artists, tracks_artists)

    def test_data_w_multiple_spaces(self):
        self.data["artist(s)_name"][3] = "Eslabon Armado,   Peso Pluma"
        artists, tracks_artists = transform.create_artists_and_tracks_artists(self.data)

        self.assertEqual(9, len(artists))
        self.assertEqual(self.expected_artists, artists)
        self.assertEqual(self.expected_tracks_artists, tracks_artists)


class TestValidateNumArtists(unittest.TestCase):
    def setUp(self):
        self.data = [["Tiltan", None], ["Naty", None], ["Kfir", None]]

    def tearDown(self):
        pass

    def test_validate_num_artists(self):
        """check number of artists fits count number"""

        result = transform.validate_num_artists(self.data, 3)
        self.assertEqual(self.data, result)

    def test_validate_num_artists_bad_count(self):
        """check valueError thrown in case number of artists doesn't fit count number"""
        with self.assertLogs("spotify_etl.transform", level='INFO') as cm:
            result = transform.validate_num_artists(self.data, 2)
            for text in cm.output:
                self.assertRegex(text, "Expected number of artists is")


class TestSplitFullName(unittest.TestCase):
    def test_first_name_and_last_name_multiple_words(self):
        """splits full name consists first and last name
         (consist more than a single word) to 2 strings"""
        result = transform.split_full_name("Tiltan   Friedman Tsur")
        self.assertEqual(2, len(result))
        self.assertEqual(["Tiltan", "Friedman Tsur"], result)

    def test_first_and_last_name(self):
        """splits full name consists first and last name to 2 strings"""
        result = transform.split_full_name("Tiltan   Friedman")
        self.assertEqual(2, len(result))
        self.assertEqual(["Tiltan", "Friedman"], result)

    def test_only_first_name(self):
        """splits full name consists only from first name. single string is expected"""
        result = transform.split_full_name("  Tiltan  ")
        self.assertEqual(1, len(result))
        self.assertEqual(["Tiltan"], result)


class TestAppendNewArtist(unittest.TestCase):
    def setUp(self):
        self.data = [["Tiltan", None], ["Naty", None], ["Kfir", None]]

    def tearDown(self):
        pass

    def test_append_new_artist_fullname(self):
        """ append new str to list,
        check return index is correct and new string added to list"""
        index = transform.append_new_artist(self.data, "Saba", "Moshe")
        self.assertEqual(index, 3)
        self.assertEqual(self.data[3][0], "Saba")
        self.assertEqual(self.data[3][1], "Moshe")

    def test_append_new_artist_first_name_only(self):
        """ append new str to list,
        check return index is correct and new string added to list"""
        index = transform.append_new_artist(self.data, "Saba")
        self.assertEqual(index, 3)
        self.assertEqual(self.data[3][0], "Saba")
        self.assertIsNone(self.data[3][1])

    def test_append_duplicate_artist_to_tail(self):
        """ append duplicate str to end of list,
        check return index is correct"""
        index = transform.append_new_artist(self.data, "Kfir")
        self.assertEqual(index, 2)

    def test_append_duplicate_artist_to_head(self):
        """ append duplicate str to start of list,
        check return index is correct"""
        index = transform.append_new_artist(self.data, "Tiltan")
        self.assertEqual(index, 0)

    def test_append_artist_to_empty_list(self):
        """ append new str to list,
        check return index is correct"""
        data = []
        index = transform.append_new_artist(data, "Tiltan")
        self.assertEqual(index, 0)
        self.assertEqual(data[0][0], "Tiltan")
        self.assertIsNone(data[0][1])


if __name__ == '__main__':
    unittest.main()
