import logging
import sys  # stdout, python version

import logging_config
import data_collection
import transform
import data_storage

assert sys.version_info >= (3, 9)

"""
training project
defined the following analysis questions:
    1.1) get artist with most listened songs
    1.2) get all his songs
    2) plot 10th most streamed singers
    3) check correlation between no. spotify playlist and apple playlists to streams 
        -- definition of good correlation - more than 75% of track_names are common

"""


def main():

    data_storage.create_db_and_store_table(transform.transform(data_collection.collect()))

    # spotify_data = db.get_data_as_dataframe("spotify")


if __name__ == '__main__':
    logging_config.configure_logger()
    app_logger = logging.getLogger("spotify_etl")
    main()

    # create numpy 2-d array without headers, check which latest song has max speech and streams
    # try use: map, filter, collection
