import pandas as pd
import numpy as np

import logging

logger = logging.getLogger("spotify_etl."+__name__)


def transform(data):
    """ transforms the data to processable data.

     Currently, splits table to 3 tables:
     Tracks, artists, and tracks-artists (needed due to many-to-many
      relationship).

     The handling of empty cells and data types is executed in data_collection module
      during pandas read_excel function call """

    return {**split_data_to_tables(data),
            "tracks": data[data.columns[~ data.columns.isin(["artist(s)_name", "artist_count"])]]}


def split_data_to_tables(data):
    artists, tracks_artists = create_artists_and_tracks_artists(data)
    return {"artists": pd.DataFrame(data=artists,
                                    columns=["first_name", "last_name"]),
            "tracks_artists": pd.DataFrame(data=tracks_artists,
                                           columns=["track_id", "artist_id"],
                                           dtype=np.int16)}


def create_artists_and_tracks_artists(data):
    """ creates 2 new lists:
     1) artists - containing all distinct artists from data
     2) tracks_artists - connects between a track and the artists crated it (one to many)"""
    artists, tracks_artists = [], []

    for track_id, artists_names in artists_record_gen(data):
        for artist in artists_names:
            artist_id = append_new_artist(artists, *split_full_name(artist))
            tracks_artists.append([track_id, artist_id])
    return artists, tracks_artists


def artists_record_gen(data):
    """generator. splits data in 'artists name' column to separate artists
    and changes capitalization to lower case.
    returns a tuple of track id and lists of its artists at each yield.
    """
    # generator not really needed in this case, because we do hold the entire lists in memory anyway,
    # but it was good practice and helped code readability
    for i, (artist_name, count_artists) in enumerate(
            zip(data["artist(s)_name"],
                data["artist_count"])):
        yield i, validate_num_artists(
            (artist_name.lower().encode('ascii', errors='ignore').decode('utf-8').split(",")),
            count_artists)


def split_full_name(full_name):
    return [my_str.strip() for my_str in full_name.lstrip().rstrip().split(" ", 1)]


def validate_num_artists(artists, expected):
    if len(artists) != expected:
        logger.error(f"Expected number of artists is {expected} (artist_count label), found {len(artists)}")
    return artists


def append_new_artist(artists, first_name, last_name=None):
    try:
        artist_id = artists.index([first_name, last_name])
    except ValueError:
        artists.append([first_name, last_name])
        artist_id = len(artists) - 1
    return artist_id
