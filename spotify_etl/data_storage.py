
from sqlalchemy import create_engine

engine = create_engine("postgresql+psycopg2://postgres:Til2013tan?@localhost/training")


def create_db_and_store_table(tables):
    with engine.begin() as conn:
        for name, pandas_df in tables.items():
            pandas_df.to_sql(name=name, con=conn, if_exists="replace")
