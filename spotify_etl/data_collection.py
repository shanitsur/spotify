import openpyxl as xl  # read and change excel
import numpy as np  # numpy dtypes
import pandas as pd  # work with dataframes
import os


def collect():

    return create_dataframe(get_abs_path_to_data("data/spotify-2023.xlsx"),
                            'spotify-2023')


def get_abs_path_to_data(rel_path):
    upper_dir = os.path.dirname(os.path.dirname(__file__))
    return os.path.join(upper_dir, rel_path)


def create_dataframe(exl_path, sheet_name):
    """read data from Excel/
    during read - define types and handles invalid cell.
     Columns with numeric types - empty/str/nan will be set to 0.

     Unfortunately, converters aren't used - found out converters always return type 'object',
      which harms future analysis. It happens even if return type specified in function signature
     """
    column_types = {"track_name": str,
                    "artist(s)_name": str,
                    "artist_count": np.uint8,
                    "released_year": str,
                    "released_month": str,
                    "released_day": str,
                    "bpm": np.uint16,
                    "key": str,
                    "mode": str,
                    "danceability_%": np.uint8,
                    "valence_%": np.uint8,
                    "energy_%": np.uint8,
                    "acousticness_%": np.uint8,
                    "instrumentalness_%": np.uint8,
                    "liveness_%": np.uint8,
                    "speechiness_%": np.uint8}
    # type_converters = {"in_spotify_playlists":_convert_column_to_uint16,
    #                 "in_spotify_charts":_convert_column_to_uint16,
    #                 "streams":_convert_column_to_uint32,
    #                 "in_apple_playlists":_convert_column_to_uint16,
    #                 "in_apple_charts":_convert_column_to_uint16,
    #                 "in_deezer_playlists":_convert_column_to_uint16,
    #                 "in_deezer_charts":_convert_column_to_uint16}
    return pd.read_excel(exl_path, sheet_name, na_values=["na"], dtype=column_types)


# not relevant if converters are not used
# def __convert_column_to_uint16(val)->np.uint16:
#     __convert_column_to_np_int_common(val, np.uint16)
#
#
# def __convert_column_to_uint8(val)->np.uint8:
#     __convert_column_to_np_int_common(val, np.uint8)
#
#
# def __convert_column_to_uint32(val)->np.uint32:
#     __convert_column_to_np_int_common(val, np.uint32)
#
#
# def __convert_column_to_int(val)->int:
#     __convert_column_to_np_int_common(val, int)
#
#
# def __convert_column_to_np_int_common(val, int_conversion_func):
#
#     if type(val) is str:
#         val_lower = val.lower()
#         if val_lower in ("", " ", "na", "nan"):
#             val = 0
#
#     try:
#         return int_conversion_func(np.floor(val))
#     except ValueError:
#         app_logger.exception(f" {val} is not the requested integer or a valid no-value str")
#
#     return int_conversion_func(0)

def load_data_dict(exl_path, sheet_name):
    """opens xlsx file and loads data into dict
    prerequisites -
    1) file must be xlsx
    2) first row must hold only headers
    3) rows >= 2 must hold only data

    Be careful - relation between columns not preserved
    """

    spotify_sheet = xl.load_workbook(exl_path)[sheet_name]

    first_row_gen = spotify_sheet.iter_rows(min_row=1,
                                            max_row=1,
                                            values_only=True)
    cols_headers = [header for row in first_row_gen for header in row]

    cols_data_gen = spotify_sheet.iter_cols(min_row=2, values_only=True)
    return {header.lower(): list(cols_data) for header, cols_data in zip(cols_headers, cols_data_gen)}
